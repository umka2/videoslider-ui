export default {
	loadThemes: async () => {
		try {
			const response = await GetAllThemes.run();
			const themes = response.data.queryTheme.reduce((acc, theme) => {
				acc[theme.theme] = theme.description;
				return acc;
			}, {});

			storeValue('themes', themes);
			console.log(appsmith.store.themes);
			// showAlert('Themes loaded successfully', 'success');
		} catch (error) {
			showAlert('Error loading themes: ' + error.message, 'error');
		}
	}
}