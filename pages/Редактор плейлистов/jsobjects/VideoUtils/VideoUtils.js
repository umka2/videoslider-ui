export default {
	extractVideoId(url) {
		const regex = /(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/(?:[^\/\n\s]+\/\S\/|(?:v|embed|watch|user)\/|\S*?[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/;
		const match = url.match(regex);
		return match ? match[1] : null;
	},
	isVideoUrlValid(url) {
		if (!url) { // Проверяем, что URL не пуст
			return false;
		}
		return this.extractVideoId(url) !== null; // Проверяем, что URL соответствует шаблону YouTube
	},
	extractVideoIds: () => {
		const fragments = appsmith.store.editable_fragments || []
		console.log("editable_fragments", fragments);
		const fragmentIds = fragments.map(x => x.fragment_id);
		console.log(fragmentIds);
		return fragmentIds;
	},
	removeWatchField: (fragments) =>  {
		return fragments.map(fragment => {
			const { watch, ...rest } = fragment;
			return rest;
		});
	},
	convertTimeToSeconds(time) {
		if (!time) {
			console.log("No time provided, returning null.");
			return null;
		}
		const parts = time.split(':');
		const seconds = (+parts[0]) * 60 * 60 + (+parts[1]) * 60 + (+parts[2]);
		console.log(`Converted time ${time} to seconds: ${seconds}`);
		return seconds;
	},
	generateYouTubeEmbedURL(videoId, startTime, endTime) {
		const start = this.convertTimeToSeconds(startTime);
		console.log(`Generating URL for videoId: ${videoId}, startTime: ${startTime}, endTime: ${endTime}`);
		let url = `https://www.youtube.com/embed/${videoId}?start=${start}&controls=0&modestbranding=1&rel=0&autoplay=1`;
		if (endTime) {
			const end = this.convertTimeToSeconds(endTime);
			url += `&end=${end}`;
		}
		console.log(`Generated URL: ${url}`);
		return url;
	},
	searchVideoFragments(videoUrl) {
		storeValue('current_video_url', videoUrl); // Сохраняем ссылку в глобальное состояние
		const video_id = this.extractVideoId(videoUrl);
		if (video_id) {
			storeValue('current_video_id', video_id);
			return GetFragmentsByVideo.run()
				.then(() => {
				const queryResult = GetFragmentsByVideo.data;
				const fragments = queryResult?.data?.queryVideo[0]?.fragments || [];

				if (fragments.length > 0) {
					const mappedFragments = fragments.map(fragment => {
						const url = this.generateYouTubeEmbedURL(fragment.video.video_id, fragment.startTime, fragment.endTime);

						return {
							fragment_id: fragment.fragment_id,
							number: fragment.number,
							startTime: fragment.startTime,
							endTime: fragment.endTime,
							title: fragment.title,
							description: fragment.description,
							theme: fragment.theme.theme,
							video_id: fragment.video.video_id,
							watch: url
						};
					});

					storeValue('editable_fragments', mappedFragments);
					storeValue('new_fragments', []);
					storeValue('isVideoInDatabase', true);
					showAlert('Video found in database.', 'success');
				} else {
					showAlert('Video not found in database. You can split and tag the video.', 'warning');
					storeValue('editable_fragments', []);
					storeValue('new_fragments', []);
					storeValue('current_video_id', video_id);
					storeValue('isVideoInDatabase', false);
				}
			})
				.catch(error => {
				showAlert('Error loading fragments: ' + error.message, 'error');
			});
		} else {
			showAlert('Invalid video URL.', 'error');
			storeValue('editable_fragments', []);
			storeValue('new_fragments', []);
			storeValue('current_video_id', null);
			storeValue('isVideoInDatabase', false);
		}
	}
}
