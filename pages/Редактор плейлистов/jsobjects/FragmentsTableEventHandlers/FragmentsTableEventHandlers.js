export default {
	handleEdit(updatedRow) {
		const isVideoInDatabase = appsmith.store.isVideoInDatabase;

		if (isVideoInDatabase) {
			// Обработка редактирования существующих фрагментов
			return UpdateFragment.run().then((response) => {
				showAlert('Fragment updated successfully', 'success');

				const updatedFragment = response.data.updateVideoFragment.videoFragment[0];

				// Получение текущих фрагментов из хранилища
				let fragments = appsmith.store.editable_fragments || [];

				// Поиск и обновление фрагмента в памяти
				fragments = fragments.map(fragment => {
					if (fragment.fragment_id === updatedFragment.fragment_id) {
						console.log("updatedFragment", updatedFragment);
						const watchURL = VideoUtils.generateYouTubeEmbedURL(
							updatedFragment.video.video_id,
							updatedFragment.startTime,
							updatedFragment.endTime
						);

						return {
							...fragment,
							startTime: updatedFragment.startTime,
							endTime: updatedFragment.endTime,
							title: updatedFragment.title,
							description: updatedFragment.description,
							theme: updatedFragment.theme.theme,
							watch: watchURL
						};
					}
					return fragment;
				});

				// Сохранение обновленного списка фрагментов в хранилище
				storeValue('editable_fragments', fragments);

				// Обновление данных в таблице
				Table1.setData(fragments);

				console.log("updatedRow.title after save", updatedFragment.title);
			}).catch(error => {
				showAlert('Error updating fragment: ' + error.message, 'error');
			});
		} else {
			// Обработка редактирования новых фрагментов локально
			let newFragments = appsmith.store.new_fragments || [];

			// Поиск и обновление фрагмента в памяти
			newFragments = newFragments.map(fragment => {
				if (fragment.fragment_id === updatedRow.fragment_id) {
					console.log("updatedFragment", updatedRow);
					const watchURL = VideoUtils.generateYouTubeEmbedURL(
						updatedRow.video_id,
						updatedRow.startTime,
						updatedRow.endTime
					);

					return {
						...fragment,
						startTime: updatedRow.startTime,
						endTime: updatedRow.endTime,
						title: updatedRow.title,
						description: updatedRow.description,
						theme: updatedRow.theme,
						watch: watchURL
					};
				}
				return fragment;
			});

			// Сохранение обновленного списка фрагментов в хранилище
			storeValue('new_fragments', newFragments);

			// Обновление данных в таблице
			Table1.setData(newFragments);

			showAlert('Fragment updated successfully', 'success');
		}
	}
}