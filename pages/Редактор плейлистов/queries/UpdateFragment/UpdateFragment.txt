mutation UpdateVideoFragment(
	$fragment_id: String!, 
	$start_time: String, 
	$end_time: String, 
	$title: String!, 
	$description: String!, 
	$theme: String!,
	$theme_description: String!
) {
	updateVideoFragment(
		input: {
			filter: {fragment_id: {eq: $fragment_id}}, 
			set: {
				startTime: $start_time, 
				endTime: $end_time,
				title: $title,
				description: $description,
				theme: {theme: $theme, description: $theme_description}
			}
		}
	) {
		videoFragment {
			fragment_id
			number
			startTime
			endTime
			title
			description
			theme {
				theme
				description
			}
			video {
				title
				video_id
			}
		}
	}
}
