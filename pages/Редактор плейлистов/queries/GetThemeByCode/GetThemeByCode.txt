query GetThemeByCode($themeCode: String!) {
  queryTheme(filter: { theme: { eq: $themeCode } }) {
    description
  }
}