query GetVideoFragmentsByVideoId($video_id: String!) {
	queryVideo(filter: { video_id: { eq: $video_id } }) {
		video_id
		title
		fragments(order: {asc: number}) {
			fragment_id
			number
			startTime
			endTime
			title
			description
			theme {
				theme
				description
			}
			video {
				video_id
			}
		}
	}
}