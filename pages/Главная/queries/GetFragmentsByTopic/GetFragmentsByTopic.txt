query GetFragmentsByTheme($theme: String!) {
  queryTheme(filter: { theme: { eq: $theme } }) {
    fragments {
      number
      startTime
      endTime
      title
      description
      video {
        video_id
        title
      }
    }
  }
}