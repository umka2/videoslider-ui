export default {
    convertTimeToSeconds(time) {
        if (!time) {
            console.log("No time provided, returning null.");
            return null;
        }
        const parts = time.split(':');
        const seconds = (+parts[0]) * 60 * 60 + (+parts[1]) * 60 + (+parts[2]);
        console.log(`Converted time ${time} to seconds: ${seconds}`);
        return seconds;
    },
    generateYouTubeEmbedURL(videoId, startTime, endTime) {
        const start = this.convertTimeToSeconds(startTime);
        console.log(`Generating URL for videoId: ${videoId}, startTime: ${startTime}, endTime: ${endTime}`);
        let url = `https://www.youtube.com/embed/${videoId}?start=${start}&controls=0&modestbranding=1&rel=0&autoplay=1`;
        if (endTime) {
            const end = this.convertTimeToSeconds(endTime);
            url += `&end=${end}`;
        }
        console.log(`Generated URL: ${url}`);
        return url;
    }
}