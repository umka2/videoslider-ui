export default {
	currentIndex: 0,

	setCurrentIndex(newIndex) {
		this.currentIndex = newIndex;
		return this.currentIndex;
	},

	resetIndex() {
		this.currentIndex = 0;
		return this.currentIndex;
	},

	nextFragment(fragments) {
		if (this.currentIndex < fragments.length - 1) {
			return this.setCurrentIndex(this.currentIndex + 1);
		}
		return this.currentIndex;
	},

	prevFragment(fragments) {
		if (this.currentIndex > 0) {
			return this.setCurrentIndex(this.currentIndex - 1);
		}
		return this.currentIndex;
	},

	getCurrentFragment(fragments) {
		return fragments[this.currentIndex];
	},

	hasNext(fragments) {
		return this.currentIndex < fragments.length - 1;
	},

	hasPrev() {
		return this.currentIndex > 0;
	}
}
