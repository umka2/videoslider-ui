export default {
	handlePrevClick() {
		const fragments = appsmith.store.fragments || [];
		if (fragments.length > 0) {
			const newIndex = VideoNavigation.prevFragment(fragments);
			storeValue('currentFragment', fragments[newIndex]);
			console.log(`Previous Index: ${newIndex}`);
			console.log('Current fragments state:', fragments);
		} else {
			console.log('No fragments available for previous click');
		}
	},

	handleNextClick() {
		const fragments = appsmith.store.fragments || [];
		if (fragments.length > 0) {
			const newIndex = VideoNavigation.nextFragment(fragments);
			storeValue('currentFragment', fragments[newIndex]);
			console.log(`Next Index: ${newIndex}`);
			console.log('Current fragments state:', fragments);
		} else {
			console.log('No fragments available for next click');
		}
	},

	isPrevDisabled() {
		const fragments = appsmith.store.fragments || [];
		return !VideoNavigation.hasPrev(fragments);
	},

	isNextDisabled() {
		const fragments = appsmith.store.fragments || [];
		return !VideoNavigation.hasNext(fragments);
	},

	handleTopicChange() {
		console.log('Topic change initiated');
		const selectedTopic = Select1.selectedOptionValue;
		console.log(`Selected topic: ${selectedTopic}`);

		VideoNavigation.resetIndex();
		console.log(`VideoNavigation index reset to: ${VideoNavigation.currentIndex}`);

		GetFragmentsByTopic.run({
			theme: selectedTopic
		}).then(() => {
			const queryResult = GetFragmentsByTopic.data;
			console.log('Query result:', queryResult);
			const queryTheme = queryResult?.data?.queryTheme;
			console.log('Query theme:', queryTheme);
			if (queryTheme && queryTheme.length > 0) {
				const fragments = queryTheme[0].fragments || [];
				storeValue('fragments', fragments);
				storeValue('currentFragment', fragments[0]);
				console.log('Fragments stored in global state:', fragments);
				console.log(`Topic changed to: ${selectedTopic}. Reset index to: ${VideoNavigation.currentIndex}`);
			} else {
				console.warn(`No fragments found for the selected topic: ${selectedTopic}`);
				storeValue('fragments', []);
				storeValue('currentFragment', null);
				console.log('Fragments stored in global state:', []);
			}
		}).catch(error => {
			console.error('Error fetching fragments:', error);
		});
	},
	handleThemesLoaded() {
		console.log('Starting to load themes');
		// Запуск асинхронного запроса для получения тем
		GetAllThemes.run().then(response => {
			console.log('Themes data loaded:', response);
			// Проверка и обработка полученных данных
			if (response && response.data && response.data.queryTheme && response.data.queryTheme.length > 0) {
				const themes = response.data.queryTheme.map(theme => ({
					label: `${theme.description} (${theme.fragmentsAggregate.count})`,
					value: theme.theme
				}));
				console.log("Themes", themes);
				storeValue('themesOptions', themes);
				console.log('Themes options stored for Select widget:', themes);
				return themes;
			} else {
				console.warn('No themes data found');
				storeValue('themesOptions', []);
				return [];
			}
		}).catch(error => {
			console.error('Error fetching themes:', error);
		})
	}
}